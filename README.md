# Angular SFC - Centre de Recyclage de l'Agglomération de Bordeaux

Bienvenue dans cette évaluation de vos compétences avec le framework Angular.

## Consignes

Cette application de suivi des Centres de Recyclage de l'Agglomération Bordelaise a été faite en un seul fichier, ce qui va compromettre son évolution et la collaboration de toute l'équipe dans le futur !

Sauvez votre équipe d'une dette technique monstrueuse en la réécrivant avec les standards du framework et ajoutez des fonctionnalités pour vos utilisateurs.

- Découpez au moins en 2 composants
- Créez une page
- Créez au moins 1 service
- Affichez la bonne couleur avec le bon statut du Centre : VERT = ouvert, ROUGE = fermé.
- Affichez les déchets refusés dans chaque Centre
- Améliorer la qualité code du projet en général
- Permettez d'afficher une liste de 3, 6 ou 9 Centres simultanément.

## Deadline

**Vous avez 1h30 pour faire cette refacto** de l'application avant que votre chef de projet lance le prochain sprint avec toute l'équipe, faites de votre mieux !

## Ressources

Votre équipe n'a pas encore scrapper toutes les informations, vous vous reposez donc sur l'Open-Data API de la ville.
[Documentation de l'API](https://opendata.bordeaux-metropole.fr/explore/dataset/dechetteries-en-temps-reel/information/)

Vous disposez d'une première mise en forme grâce à Tailwindcss et DaisyUI.
