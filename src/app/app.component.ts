import { HttpClient, HttpParams } from "@angular/common/http";
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import { map, tap } from "rxjs";

/**
 * Leaflet Map import section
 */
import * as L from "leaflet";

const iconRetinaUrl = "assets/marker-icon-2x.png";
const iconUrl = "assets/marker-icon.png";
const shadowUrl = "assets/marker-shadow.png";
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41],
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: "app-root",
  template: `
    <main class="mx-auto w-9/12">
      <h1 class="text-xl text-center">
        Centre de recyclage - Bordeaux Métropole
      </h1>

      <!-- List items -->
      <section *ngIf="!!centers" class="mt-6">
        <article class="flex flex-wrap justify-between gap-5">
          <div class="card w-96 bg-base-100 shadow-xl">
            <div class="card-body">
              <h2 class="card-title">{{ centers[0].nom }}</h2>
              <p>{{ centers[0].adresse }}</p>
              <div class="card-actions justify-end">
                <button class="btn btn-primary" (click)="view(centers[0])">
                  Voir
                </button>
              </div>
            </div>
          </div>
          <div class="card w-96 bg-base-100 shadow-xl">
            <div class="card-body">
              <h2 class="card-title">{{ centers[1].nom }}</h2>
              <p>{{ centers[2].adresse }}</p>
              <div class="card-actions justify-end">
                <button class="btn btn-primary" (click)="view(centers[1])">
                  Voir
                </button>
              </div>
            </div>
          </div>
          <div class="card w-96 bg-base-100 shadow-xl">
            <div class="card-body">
              <h2 class="card-title">{{ centers[2].nom }}</h2>
              <p>{{ centers[2].adresse }}</p>
              <div class="card-actions justify-end">
                <button class="btn btn-primary" (click)="view(centers[2])">
                  Voir
                </button>
              </div>
            </div>
          </div>
        </article>
        <div class="flex justify-between items-center">
          <div class="form-control w-full max-w-xs">
            <label class="label">
              <span class="label-text">Nombre de centre par page</span>
            </label>
            <select class="select select-sm select-bordered w-full max-w-xs">
              <option>3</option>
              <option>6</option>
              <option>9</option>
            </select>
          </div>
          <p>{{ totalCenters }} centres</p>
        </div>
      </section>

      <!-- Single Item -->
      <section *ngIf="detailedCenter" class="mt-9">
        <div class="card card-map bg-base-100 shadow-xl">
          <!-- Maps -->
          <div class="map-container">
            <div class="map-frame">
              <div id="map"></div>
            </div>
          </div>
          <div class="card-body">
            <h2 class="card-title">
              {{ detailedCenter?.nom }}
              <div class="badge badge-success gap-2">
                {{ detailedCenter?.statut }}
              </div>
            </h2>
            <p>{{ detailedCenter?.adresse }}</p>
            <div class="overflow-x-auto">
              <table class="table table-compact w-full">
                <!-- head -->
                <thead>
                  <tr>
                    <th>Déchets acceptés</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- row 1 -->
                  <tr>
                    <td>{{ detailedCenter?.acceptes[0] }}</td>
                  </tr>
                  <!-- row 2 -->
                  <tr>
                    <td>{{ detailedCenter?.acceptes[1] }}</td>
                  </tr>
                  <!-- row 3 -->
                  <tr>
                    <td>{{ detailedCenter?.acceptes[2] }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
    </main>
  `,
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit, AfterViewInit {
  detailedCenter;
  centers;
  totalCenters;
  private map;

  constructor(private http: HttpClient, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.getCenters();
  }

  ngAfterViewInit(): void {}

  /**
   * Fetch Centers on Open-data API
   */
  getCenters() {
    const params = new HttpParams()
      .set("dataset", "dechetteries-en-temps-reel")
      .set("rows", "3");

    this.http
      .get("https://opendata.bordeaux-metropole.fr/api/records/1.0/search/", {
        params,
      })
      .pipe(
        tap((response: any) => (this.totalCenters = response.nhits)),
        map((response: any) => response.records.map((record) => record.fields)),
        map((records) => {
          return records.map((record) => {
            return {
              ...record,
              acceptes: record.acceptes.split(";"),
            };
          });
        })
      )
      .subscribe((response) => {
        this.centers = response;
      });
  }

  /**
   * Add center to current detailedCenter
   * @param center
   */
  view(center) {
    this.detailedCenter = center;
    this.cdr.detectChanges();
    this.initMap();
  }

  /**
   * Define Leaflet map with options
   */
  initMap(): void {
    if (this.map) {
      this.map.remove();
    }

    this.map = L.map("map", {
      center: this.detailedCenter.geo_point_2d,
      zoom: 18,
    });

    const tiles = L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        maxZoom: 18,
        minZoom: 1,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }
    );
    const marker = L.marker(this.detailedCenter.geo_point_2d);

    tiles.addTo(this.map);
    marker.addTo(this.map);
  }
}
